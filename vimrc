set nocompatible                                  " just an improvement

" vvv  PLUGINS  vvv
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'bling/vim-airline'
Plugin 'airblade/vim-gitgutter'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/syntastic'
Plugin 'justinmk/vim-sneak'
call vundle#end()

" vvv  GENERAL OPTIONS  vvv
filetype plugin indent on
syntax on
highlight LineNr ctermfg=darkgray
set lazyredraw                                    " be lazy and faster
set relativenumber                                " <^line numbers
set hlsearch | set incsearch | set gdefault       " search
set tabstop=2 | set shiftwidth=2 | set smarttab   " tabs
set mouse=a | set ttymouse=xterm2                 " mouse
set wildmenu | set wildmode=full                  " menus
set shortmess=I                                   " no startup msg
set laststatus=2                                  " status line always visible
set showcmd                                       " show commands - bottom right
set autoread                                      " reload automatically
let g:netrw_banner=0                              " hiding the banner
let g:netrw_liststyle=3                           " tree-style
set backupdir=~/.vim/tmp,.                        " *~: try tmp dir first
set directory=~/.vim/tmp,.                        " *.swp: try tmp dir first

" vvv  KEY (RE)MAPPINGS  vvv
" movement inside a wrapped line
nnoremap j gj
nnoremap k gk
" swap ; and :
nnoremap ; :
nnoremap : ;
" tabs & buffer navigation
noremap <left> gT
noremap <right> gt
noremap <up> :bn<cr>
noremap <down> :bp<cr>
" easy split navigation (C-l is kinda special)
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-h> <C-w>h
noremap <C-l> <C-w>l<C-l>:nohl<cr>
" :te <file> opens in new tab
cabbrev te tabedit
cabbrev w!! w !sudo dd of=%

" vvv  LEADER MAPPINGS  vvv
" ,: leader key
" N: switching between rel and abs line numbers
" C: czech keymap
" D: dvorak keymap
" P: paste mode
" R: reste term & nohl
let mapleader=','
noremap <Leader>n :if(&nu)\|se rnu\|el\|se nu\|en<cr>:se rnu?<cr>
noremap <Leader>c :if &kmp == 'czech'\|se kmp=\|el\|se kmp=czech\|en<cr>:set kmp?<cr>
noremap <Leader>d :if &kmp == 'dvorak'\|se kmp=\|el\|se kmp=dvorak\|en<cr>:set kmp?<cr>
noremap <Leader>p :set paste!<cr>:echo<cr>
noremap <Leader>r :nohl<cr><C-l><cr>

" vvv  VARIOUS PLUGIN TUNING  vvv
" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='serene'
" gitgutter
highlight clear SignColumn
highlight GitGutterAdd ctermfg=green guifg=darkgreen
highlight GitGutterChange ctermfg=yellow guifg=darkyellow
highlight GitGutterDelete ctermfg=red guifg=darkred
highlight GitGutterChangeDelete ctermfg=yellow guifg=darkyellow
" sneak
let g:sneak#streak = 1
" syntastic
let g:syntastic_javascript_checkers = ['jsxhint']
let g:syntastic_check_on_open = 1
