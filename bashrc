# vvv  ENVIRONMENT  vvv

export PATH=$PATH:~/bin
export EDITOR=vim

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8



# vvv  INTERACTIVE  vvv

[[ $- != *i* ]] && return

alias ll="ls -l"
alias la="ls -a"
alias lla="ls -la"
alias vi="vim"
alias g="sudo bash"

if [ $EUID = 0 ] ; then
	PS1='$? \e[41m[\u@\h:\w]\e[0m\$ '
else
	PS1='$? \e[1;36m[\u@\h:\w]\e[0m\$ '
fi

case `uname` in
	'Darwin')
		alias ls="ls -G"
		alias vim="/Applications/MacVim.app/Contents/MacOS/Vim"
		. /usr/local/etc/bash_completion.d/git-completion.bash
		;;
	'Linux')
		alias ls="ls --color"
		;;
esac
